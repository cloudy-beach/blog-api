'use strict'

module.exports = {
    testEnvironment: 'node',
    setupFilesAfterEnv: ["jest-extended/all"],
    collectCoverage: true,
    transformIgnorePatterns: ['<rootDir>/node_modules/'],
    coverageReporters: [
        'json',
        'html',
    ],
    // watchman: false,
}
