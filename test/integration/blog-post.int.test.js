const request = require('supertest')
const app = require('../../app.local')

const uri = '/blogs/posts/'
const createUri = '/blogs/myBlog/posts'
const newBlogPost = require('../fixture/new-blog-post.json')

//TODO TBU validate that the ID is a valid mongoID -- client error
// const invalidId = '000'
const unknownPostId = '4edd40c86762e0fb12000003'


//TODO user journey test
// POST should post a blog post, and cache the post (twice?)
// GET should return 200 and blog post for cached ID
// get list should return all blog posts (two were created)

afterAll( () => {
    const mongoDB = require('mongoose')
    mongoDB.connection.close()
})
describe('User journey for handling a single blog post', () => {
    let theBlogPost
    const agent = request.agent(app)

    function thenResponseMatchesTheBlogPostAndHasStatus(res, status = 200) {
        expect(res.statusCode).toBe(status)
        // console.log(res.body)
        expect(res.body.title).toStrictEqual(theBlogPost.title)
        expect(res.body.content).toStrictEqual(theBlogPost.content)
        expect(res.body.blogKey).toBeTruthy()
    }

    function thenResponseMatchesUpdatedContentAndHasStatus(res, newContent, status = 200) {
        expect(res.statusCode).toBe(status)
        console.log(res.body)
        expect(res.body.title).toStrictEqual(theBlogPost.title)
        expect(res.body.content).toStrictEqual(newContent)
    }


    it('should create the blog post', async () => {
        theBlogPost = newBlogPost
        const res = await agent.post(createUri).send(newBlogPost)
        thenResponseMatchesTheBlogPostAndHasStatus(res, 201);
        theBlogPost = res.body
    })

    it('should find the blog post', async function () {
        const res = await agent.get(uri.concat(theBlogPost._id)).send()
        thenResponseMatchesTheBlogPostAndHasStatus(res, 200)
        theBlogPost = res.body
    })

    it('should update the blog post', async function () {
        const updatedContent = {
            content: "new content"
        }
        const res = await agent.put(uri.concat(theBlogPost._id)).send(updatedContent)
        thenResponseMatchesUpdatedContentAndHasStatus(res, updatedContent.content, 200)
        theBlogPost = res.body
    })

    it('should delete the blog post', async function () {
        const res = await agent.delete(uri.concat(theBlogPost._id)).send()
        thenResponseMatchesTheBlogPostAndHasStatus(res, 200)
        theBlogPost = null
    })
})

describe('BlogPost API - client errors', () => {

    const agent = request.agent(app)

    test('GET should return 404 for unknown ID',  async() =>{
        const response = await agent.get(uri.concat(unknownPostId)).send()
        expect(response.status).toBe(404)
    })
    test('PUT should return 404 for unknown ID', async() => {
        const response = await agent.put(uri.concat(unknownPostId)).send({content: "FooBar"})
        expect(response.status).toBe(404)
    })
    test('DELETE should return 404 for unknown ID', async () => {
        const response = await request(app).delete(uri.concat(unknownPostId)).send()
        expect(response.status).toBe(404)
    })
})
