'use strict'

const httpMocks = require('node-mocks-http')

//load testee
const BlogPostController = require('../../src/blogs/blog-post-controller')

//service mocks
const {BlogPost} = require('../../src/blogs/blog-post-model')

//data fixture
const blogKey = "myBlog"

const blogPost = require('../fixture/blog-post.json')
const newBlogPost = require('../fixture/new-blog-post.json')
const updatedBlogPost =require('../fixture/updated-blog-post.json')
jest.mock('../../src/blogs/blog-post-model')


let req, res, next;

beforeEach(() => {
    jest.resetAllMocks()
    req = httpMocks.createRequest()
    res = httpMocks.createResponse()
    next = jest.fn()
})

describe('Find blog post by id', () => {
    it('should have a function findById', () => {
        expect(typeof BlogPostController.findBlogPostById).toBe('function')
    })
    it('should return HttpCode 200 with blog post', async function () {
        BlogPost.findById.mockReturnValue(blogPost);
        req.params.id = blogPost.id

        await BlogPostController.findBlogPostById(req, res, next)

        expect(res.statusCode).toBe(200)
        expect(res._getJSONData()).toStrictEqual(blogPost)
        expect(res._isEndCalled()).toBeTruthy()
        expect(BlogPost.findById).toHaveBeenCalledWith(blogPost.id)
    })
    it('should create ItemNotFoundError when blog post does not exist', async function () {

        await BlogPostController.findBlogPostById(req, res, next)

        expect(next).toHaveBeenCalledOnce()
        const error= next.mock.calls[0][0]
        expect(error.status).toBe(404)
        expect(error.name).toBe('Item not found exception')
    })
    it('should handle errors', async function () {
        const errorMessage = {message: "an error"};
        const error = Promise.reject(errorMessage);
        BlogPost.findById.mockReturnValue(error);

        await BlogPostController.findBlogPostById(req, res, next)

        expect(next).toBeCalledWith(errorMessage)
    })
})

describe('Create blog post in blog "myBlog"', () => {
    it('should have a function findById', () => {
        expect(typeof BlogPostController.createBlogPost).toBe('function')
    })
    it('should create an new blog post with code 201', async function () {
        BlogPost.create.mockReturnValue(blogPost)
        req.body = newBlogPost
        req.params.blog = blogKey

        await BlogPostController.createBlogPost(req, res, next)

        expect(res.statusCode).toBe(201)
        expect(res._getJSONData()).toStrictEqual(blogPost)
        expect(res._isEndCalled()).toBeTruthy()

        const createdArg = BlogPost.create.mock.calls[0][0]
        expect(createdArg.blogKey).toBe(blogKey)
        expect(createdArg.title).toBe(newBlogPost.title)
        expect(createdArg.content).toBe(newBlogPost.content)
    })
    it('should handle errors', async function () {
        const errorMessage = {message: "an error"}
        const error = Promise.reject(errorMessage)
        BlogPost.create.mockReturnValue(error)

        await BlogPostController.createBlogPost(req, res, next)

        expect(next).toBeCalledWith(errorMessage)
    })
})

describe('Update blog post', () => {
    it('should have a function updateBlogPost', async function () {
        expect(typeof BlogPostController.updateBlogPost).toBe('function')
    })
    it('should update a blog post and return code 200', async function () {
        BlogPost.findByIdAndUpdate.mockReturnValue(updatedBlogPost)
        req.body = updatedBlogPost

        await BlogPostController.updateBlogPost(req, res, next)

        expect(res.statusCode).toBe(200)
        expect(res._getJSONData()).toStrictEqual(updatedBlogPost)
        expect(res._isEndCalled()).toBeTruthy()
    })
    it('should create ItemNotFoundError when blog post does not exist', async function () {

        await BlogPostController.updateBlogPost(req, res, next)

        expect(next).toHaveBeenCalledOnce()
        const error= next.mock.calls[0][0]
        expect(error.status).toBe(404)
        expect(error.name).toBe('Item not found exception')
    })
    it('should handle errors', async function () {
        const errorMessage = {message: "an error"}
        const error = Promise.reject(errorMessage)
        //return error from mongoose
        BlogPost.findByIdAndUpdate.mockReturnValue(error)

        await BlogPostController.updateBlogPost(req, res, next)
        expect(next).toBeCalledWith(errorMessage)
     })
})

describe('Delete blog post', () => {
    it('should have a function deleteBlogPost', () => {
        expect(typeof BlogPostController.deleteBlogPost).toBe('function')
    })
    it('should delete a blog post and return code 200', async function () {
        BlogPost.findByIdAndDelete.mockReturnValue(blogPost)
        req.params.id = blogPost.id

        await BlogPostController.deleteBlogPost(req, res, next)

        expect(res.statusCode).toBe(200)
        expect(res._getJSONData()).toStrictEqual(blogPost)
        expect(res._isEndCalled()).toBeTruthy()
        expect(BlogPost.findByIdAndDelete).toHaveBeenCalledExactlyOnceWith(blogPost.id)
    })
    it('should create ItemNotFoundError when blog post does not exist', async function () {

        await BlogPostController.deleteBlogPost(req, res, next)

        expect(next).toHaveBeenCalledOnce()
        const error= next.mock.calls[0][0]
        expect(error.status).toBe(404)
        expect(error.name).toBe('Item not found exception')
    })
    it('should handle errors', async function () {
        const errorMessage = {message: "an error"}
        const error = Promise.reject(errorMessage)
        BlogPost.findByIdAndDelete.mockReturnValue(error)

        await BlogPostController.deleteBlogPost(req, res, next)

        expect(next).toBeCalledWith(errorMessage)
     })
})
