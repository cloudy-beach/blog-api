'use strict'

const httpMocks = require('node-mocks-http')
const {errorHandler, errorLogging} = require('../../src/middleware/error-handling')

let req, res, next

describe('Middleware for error handling', () => {
    describe('Should return error messages as API response', () => {
        beforeEach(()=>{
            req = httpMocks.createRequest()
            res = httpMocks.createResponse()
            next = jest.fn()
        })

        it('should return 500 code', async function () {
            const err = new Error('an error')
            errorHandler(err, req, res, next)
            expect(res.statusCode).toBe(500)
            expect(res._isEndCalled()).toBeTruthy()
        })
        it('should return error message', async function () {
            const err = new Error('another error')
            err.name = 'Internal Server Error'
            const expectedBody = {
                path: "",
                error: "Internal Server Error: another error"
            }
            errorHandler(err, req, res, next)
            expect(res._getJSONData()).toStrictEqual(expectedBody)
        })
        it('should use status of the error IFF defined', async function () {
            const err = new Error('booar')
            err.status = 400
            errorHandler(err, req, res, next)
            expect(res.statusCode).toBe(400)
        })
        it('should delegate to express if response has been sent', () =>{
            res.status(500).send() //simulate response
            const err = new Error('')

            errorHandler(err, req, res, next)

            expect(next).toHaveBeenCalledExactlyOnceWith(err)
        })
    })
    describe('Should log errors', () => {
        next = jest.fn()
        const err = new Error('foobar')
        errorLogging(err, req, res, next)
        expect(next).toHaveBeenCalledExactlyOnceWith(err)
    })
})
