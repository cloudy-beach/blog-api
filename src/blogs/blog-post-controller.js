'use strict'

const {BlogPost} = require("./blog-post-model")
const ItemNotFoundError = require("./exceptions");

function handleError(err, next) {
    next(err)
}

function respondWith(blogPost, status, req, res, next) {
    if(blogPost) {
        res.status(status).json(blogPost).send()
    } else {
        handleError( new ItemNotFoundError('Blog post', req.params.id), next)
    }
}

async function findBlogPostById(req, res, next) {
    try {
        const blogPost = await BlogPost.findById(req.params.id)
        respondWith(blogPost, 200, req, res, next)
    } catch (err) {
        handleError(err, next)
    }
}

async function createBlogPost(req, res, next){
    try {
        const newBlogPost = {
            ...req.body,
            blogKey: req.params.blog
        }
        const blogPost = await BlogPost.create(newBlogPost)
        respondWith(blogPost, 201, req, res, next)
    } catch (err) {
        handleError(err, next)
    }
}

async function updateBlogPost(req, res, next) {
    try {
        const blogPost = await BlogPost.findByIdAndUpdate(req.params.id, req.body, {new: true, useFindAndModify: false})
        respondWith(blogPost, 200, req, res, next)
    } catch (err) {
        handleError(err, next)
    }
}

async function deleteBlogPost(req, res, next) {
    try {
        const blogPost = await BlogPost.findByIdAndDelete(req.params.id)
        respondWith(blogPost, 200, req, res, next)
    } catch (err) {
        handleError(err, next)
    }
}

module.exports = {
    findBlogPostById,
    createBlogPost,
    updateBlogPost,
    deleteBlogPost,
}
