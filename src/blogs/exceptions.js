'use strict'

function toMessage(itemName, id) {
 return `${itemName || 'Item'} with id ${id} not found.`
}

class ItemNotFoundError extends Error {
    constructor(itemName, id) {
        super(toMessage(itemName, id))
        this.name = 'Item not found exception'
        this.status = 404
    }
}

module.exports = ItemNotFoundError
