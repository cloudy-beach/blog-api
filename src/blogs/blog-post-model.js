'use strict'

const mongoose = require('mongoose')

const blogPostSchema = new mongoose.Schema({
    "title" : {
        type: String,
        require: true
    },
    "content" : {
        type: String,
        require: true
    },
    "blogKey" : {
        type: String,
        require: true,
        index: true
    }
})

module.exports = {
    BlogPost: mongoose.model('BlogPost', blogPostSchema),
}
