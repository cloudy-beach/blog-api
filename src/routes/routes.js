const express = require('express')

const routerPosts = express.Router()
const routerBlogs = express.Router()
const BlogPostController = require('../blogs/blog-post-controller')

routerPosts.get('/:id', BlogPostController.findBlogPostById)
routerPosts.put('/:id', BlogPostController.updateBlogPost)
routerPosts.delete('/:id', BlogPostController.deleteBlogPost)

routerBlogs.post('/:blog/posts', BlogPostController.createBlogPost)

module.exports = {routerBlogs, routerPosts}
