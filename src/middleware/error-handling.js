'use strict'

function logError(err, req, res, next) {
    console.log(err)
    next(err)
}

function toResponseBody(req, err) {//err.name.concat(': ', err.message) {
    return {
        path: req.path,
        error: `${err.name}: ${err.message}`
    };
}

function handleError(err, req, res, next) {
    if (res.headersSent) {
        return next(err)
    }
    const error =toResponseBody(req, err);
    const statusCode = err.status || 500
    res.status(statusCode).json(error).send()
}

module.exports = {
    errorHandler: handleError,
    errorLogging: logError,
}
