'use strict'

const mongoose = require("mongoose");

async function connect() {
    try{
        await mongoose.connect("mongodb://mongo_user:example@localhost:27017");
        console.log("Connected to MongoDB")
    } catch (err) {
        console.log("Error connecting to MongoDB");
        console.log(err);
    }
}

module.exports = {connect};
