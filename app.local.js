'use strict'

const express = require('express')
const {routerPosts, routerBlogs} = require('./src/routes/routes.js')
const {errorLogging, errorHandler} = require('./src/middleware/error-handling')
const mongoConnection = require('./src/config/mongo.connect')

mongoConnection.connect()

const app = express()

app.use(express.json())
app.use('/blogs', routerBlogs)
app.use('/blogs/posts', routerPosts)

app.use(errorLogging)
app.use(errorHandler)

app.listen(3000)

module.exports = app
