# Blog-API
Demo project to play around with express, mongoose, and redis.
Stores blog articles.
Plays around with custom error handling.

### Setup

- Run `npm install` in the root of the project to install server dependencies
- Run `docker compose up` to start a mongo database and a redis cache
- Change back into the root of the project and run `npm run start` to start the server or `npm run server` to run the server with nodemon
- Access the application at `localhost:8080` in your browser

**Important:**
This is only a demo for local development. No configuration for remote deployment is included. No auth included.

## Developer notes

- express 5 (currently in beta) hands over any Promise.rejected to the next function automatically. Error handling on the controller level should be reworked.

# Features

* Create new blog posts in a blog
* Get, update, and delete existing blog posts

## TODOs

- use REDIS to handle caching
- introduce a status enum for blog posts: draft -> published (optional)
- validate the input data for a blog post (is it in a good format? throw error if not)
- retrieve blog posts by blog
